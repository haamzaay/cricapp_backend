<?php

namespace App\Http\Controllers;

use App\Modules\Match\Contracts\MatchRepository;
use App\Modules\ScoreCard\Contracts\ScoreCardRepository;
use App\Modules\ScoreCard\ScoreCard;
use App\Modules\Scoring\Scoring;
use Illuminate\Http\Request;

class MatchController extends Controller
{
    /**
     * @var MatchRepository
     */
    private $matchRepository;
    /**
     * @var ScoreCardRepository
     */
    private $scoreCardRepository;

    /**
     * MatchController constructor.
     * @param MatchRepository $matchRepository
     * @param ScoreCardRepository $scoreCardRepository
     */
    public function __construct(MatchRepository $matchRepository, ScoreCardRepository $scoreCardRepository)
    {
        $this->matchRepository = $matchRepository;
        $this->scoreCardRepository = $scoreCardRepository;
    }

    public function getScoreCardForBatsmen(Request $request) {
       $data = $this->scoreCardRepository->getScoreCardByMatchIdBatsmen($request->id);
       return response()->json($data);
    }

    public function getScoreCardForBowling(Request $request) {
        $data = $this->scoreCardRepository->getScoreCardByMatchIdBowler($request->id);
        return response()->json($data);
    }

    public function getMatches() {
        $data = $this->matchRepository->getAll();
        return response()->json($data);
    }
}
