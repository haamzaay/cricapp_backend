<?php


namespace App\Modules\BowlingCard;


use App\Modules\Player\Player;
use App\Modules\Scoring\Scoring;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BowlingCard
 * @package App\Modules\BowlingCard
 */
class BowlingCard extends Model
{
    /**
     * @var string
     */
    protected $table = 'bowling_card';

    /**
     * @var array
     */
    protected $fillable = ['player_id', 'overs', 'maiden', 'wicket', 'four', 'sixes'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scoring() {
        return $this->hasMany(Scoring::class, 'bowling_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player() {
        return $this->belongsTo(Player::class, 'player_id');
    }
}