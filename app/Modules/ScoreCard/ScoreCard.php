<?php


namespace App\Modules\ScoreCard;


use App\Modules\Scoring\Scoring;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ScoreCard
 * @package App\Modules\Scorecard
 */
class ScoreCard extends Model
{
    /**
     * @var string
     */
    protected $table = 'score_card';

    /**
     * @var array
     */
    protected $fillable = ['innings', 'match_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scoring() {
        return $this->hasMany(Scoring::class, 'score_card_id');
    }

}