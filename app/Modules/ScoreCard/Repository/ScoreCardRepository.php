<?php


namespace App\Modules\ScoreCard\Repository;
use App\Modules\ScoreCard\Contracts\ScoreCardRepository as ScoreCardRepositoryContract;
use App\Modules\ScoreCard\ScoreCard;

class ScoreCardRepository implements ScoreCardRepositoryContract
{
    /**
     * @var ScoreCard
     */
    private $model;

    /**
     * ScoreCardRepository constructor.
     * @param ScoreCard $model
     */
    public function __construct(ScoreCard $model)
    {
        $this->model = $model;
    }

    public function getScoreCard()
    {
        return $this->model->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getScoreCardByMatchId($id)
    {
       return $this->model->where('match_id', $id)->with('scoring')->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getScoreCardByMatchIdBowler($id)
    {
        return $this->model->where('match_id', $id)->with('scoring.bowling.player')->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getScoreCardByMatchIdBatsmen($id)
    {
        return $this->model->where('match_id', $id)->with('scoring.batting.player')->get();
    }
}