<?php


namespace App\Modules\ScoreCard\Contracts;


/**
 * Interface ScoreCardRepository
 * @package App\Modules\ScoreCard\Contracts
 */
interface ScoreCardRepository
{
    /**
     * @return mixed
     */
    public function getScoreCard();

    /**
     * @param $id
     * @return mixed
     */
    public function getScoreCardByMatchId($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getScoreCardByMatchIdBowler($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getScoreCardByMatchIdBatsmen($id);


}