<?php


namespace App\Modules\Match;


use App\Modules\Team\Team;
use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $table = 'match';

    protected $fillable = ['team1_id', 'team2_id', 'tournament_id'];

    public function teamOne() {
       return $this->belongsTo(Team::class, 'team1_id');
    }

    public function teamTwo() {
       return $this->belongsTo(Team::class, 'team2_id');
    }
}