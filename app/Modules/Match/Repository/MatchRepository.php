<?php


namespace App\Modules\Match\Repository;
use \App\Modules\Match\Contracts\MatchRepository as MatchRepositoryContract;
use App\Modules\Match\Match;


class MatchRepository implements MatchRepositoryContract
{
    /**
     * @var Match
     */
    private $model;

    public function __construct(Match $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
         return $this->model->with('teamOne', 'teamTwo')->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        // TODO: Implement getById() method.
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        // TODO: Implement create() method.
    }

    /**
     * @param $id
     * @param array $attribute
     * @return mixed
     */
    public function update($id, array $attribute)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }
}