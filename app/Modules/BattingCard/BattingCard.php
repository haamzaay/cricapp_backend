<?php


namespace App\Modules\BattingCard;


use App\Modules\Player\Player;
use App\Modules\Scoring\Scoring;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BattingCard
 * @package App\Modules\BattingCard
 */
class BattingCard extends Model
{
    /**
     * @var string
     */
    protected $table = 'batting_card';

    /**
     * @var array
     */
    protected $fillable = ['player_id', 'runs', 'ball', 'four', 'sixes', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function batting() {
        return $this->hasMany(Scoring::class, 'batting_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player() {
        return $this->belongsTo(Player::class, 'player_id');
    }


}