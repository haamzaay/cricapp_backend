<?php


namespace App\Modules\Team;


use App\Modules\Match\Match;
use App\Modules\TeamPlayer\TeamPlayer;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Team
 * @package App\Modules\Team
 */
class Team extends Model
{
    /**
     * @var string
     */
    protected $table = 'team';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function TeamPlayer() {
        return $this->hasMany(TeamPlayer::class, 'player_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function MatchOne() {
        return $this->hasMany(Match::class, 'team1_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function MatchTwo() {
        return $this->hasMany(Match::class, 'team2_id');
    }
}