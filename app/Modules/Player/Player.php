<?php


namespace App\Modules\Player;


use App\Modules\BattingCard\BattingCard;
use App\Modules\BowlingCard\BowlingCard;
use App\Modules\TeamPlayer\TeamPlayer;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Player
 * @package App\Modules\Player
 */
class Player extends Model
{
    /**
     * @var string
     */
    protected $table = 'player';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     *
     */
    public function TeamPlayer() {
        $this->hasMany(TeamPlayer::class, 'player_id');
    }

    public function battingCard() {
        $this->hasMany(BattingCard::class, 'player_id');
    }

    public function bowlingCard() {
        $this->hasMany(BowlingCard::class, 'player_id');
    }
}