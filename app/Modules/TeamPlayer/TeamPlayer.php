<?php


namespace App\Modules\TeamPlayer;


use App\Modules\Player\Player;
use App\Modules\Team\Team;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TeamPlayer
 * @package App\Modules\TeamPlayer
 */
class TeamPlayer extends Model
{
    /**
     * @var string
     */
    protected $table = 'team_player';

    /**
     * @var array
     */
    protected $fillable = ['player_id', 'team_id'];

    /**
     *
     */
    public function player() {
        $this->belongsTo(Player::class, 'player_id');
    }

    /**
     *
     */
    public function team() {
        $this->belongsTo(Team::class, 'team_id');
    }
}