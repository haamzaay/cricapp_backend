<?php


namespace App\Modules\Tournament;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Tournament
 * @package App\Modules\Tournament
 */
class Tournament extends Model
{
    /**
     * @var string
     */
    protected $table = 'tournament';

    /**
     * @var array
     */
    protected $fillable = ['name'];
}