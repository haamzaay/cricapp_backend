<?php


namespace App\Modules\Scoring;


use App\Modules\BattingCard\BattingCard;
use App\Modules\BowlingCard\BowlingCard;
use App\Modules\ScoreCard\ScoreCard;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Scoring
 * @package App\Modules\Scoring
 */
class Scoring extends Model
{
    /**
     * @var string
     */
    protected $table = 'scoring';

    /**
     * @var array
     */
    protected $fillable = ['bowling_id', 'batting_id', 'score_card_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bowling() {
        return $this->belongsTo(BowlingCard::class, 'bowling_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function batting() {
        return $this->belongsTo(BattingCard::class, 'batting_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scoreCard() {
        return $this->belongsTo(ScoreCard::class, 'score_card_id');
    }
}