<?php

namespace App\Providers;

use App\Modules\Match\Repository\MatchRepository;
use App\Modules\ScoreCard\Contracts\ScoreCardRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\App\Modules\Match\Contracts\MatchRepository::class, MatchRepository::class);
        $this->app->singleton(ScoreCardRepository::class,  \App\Modules\ScoreCard\Repository\ScoreCardRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
