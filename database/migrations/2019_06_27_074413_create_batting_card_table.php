<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBattingCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batting_card', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id')->unsigned();
            $table->integer('runs')->default(0);
            $table->integer('ball')->default(0);
            $table->integer('four')->default(0);
            $table->integer('sixes')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
        });

        Schema::table('batting_card', function (Blueprint $table) {
           $table->foreign('player_id')->on('player')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batting_card');
    }
}
