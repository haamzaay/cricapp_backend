<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoringTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scoring', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('score_card_id')->unsigned();
            $table->integer('bowling_id')->unsigned();
            $table->integer('batting_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('scoring', function (Blueprint $table) {
            $table->foreign('score_card_id')->references('id')->on('score_card');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scoring');
    }
}
