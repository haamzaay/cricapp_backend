<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchWicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_wicket', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match_id')->unsigned();
            $table->integer('bowler_id')->unsigned();
            $table->integer('batsmen_id')->unsigned();
            $table->string('type', 255);
            $table->timestamps();
        });

        Schema::table('match_wicket', function (Blueprint $table) {
           $table->foreign('match_id')->on('match')->references('id');
           $table->foreign('bowler_id')->on('player')->references('id');
           $table->foreign('batsmen_id')->on('player')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_wicket');
    }
}
