<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use \App\Modules\BowlingCard\BowlingCard;

$factory->define(BowlingCard::class, function (Faker $faker) {
    return [
        'player_id' => \App\Modules\Player\Player::all()->random()->id,
        'overs' => 10,
        'maiden' => $faker->randomDigit,
        'wicket' => $faker->randomDigit,
        'four' => $faker->randomDigit,
        'sixes' => $faker->randomDigit,
    ];
});
