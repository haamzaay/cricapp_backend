<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Modules\Scoring\Scoring::class, function (Faker $faker) {
    return [
        'score_card_id' => \App\Modules\ScoreCard\ScoreCard::all()->random()->id,
        'bowling_id' => \App\Modules\BowlingCard\BowlingCard::all()->random()->id,
        'batting_id' => \App\Modules\BattingCard\BattingCard::all()->random()->id,
    ];
});
