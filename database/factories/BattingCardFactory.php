<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\Modules\BattingCard\BattingCard;

$factory->define(BattingCard::class, function (Faker $faker) {
    return [
        'player_id' => \App\Modules\Player\Player::all()->random()->id,
        'runs' => $faker->randomDigit,
        'ball' => $faker->randomDigit,
        'four' => $faker->randomDigit,
        'sixes' => $faker->randomDigit,
        'status' => $faker->randomDigit,
    ];
});
