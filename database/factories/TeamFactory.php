<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Modules\Team\Team::class, function (Faker $faker) {
    return [
        'name'=> $faker->unique()->randomElement(['Pakistan', 'England', 'Australia', 'India']),
    ];
});
