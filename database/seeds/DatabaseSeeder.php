<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Modules\Player\Player::class, 44)->create();
        factory(\App\Modules\Team\Team::class, 4)->create();
        factory(\App\Modules\Tournament\Tournament::class)->create();
        factory(\App\Modules\BowlingCard\BowlingCard::class,20)->create();
        factory(\App\Modules\BattingCard\BattingCard::class,20)->create();
        $this->call(TeamPlayers::class);
        $this->call(MatchSeeder::class);
        factory(\App\Modules\Scoring\Scoring::class,100)->create();

    }
}
