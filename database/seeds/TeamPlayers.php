<?php

use Illuminate\Database\Seeder;

class TeamPlayers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $teams = App\Modules\Team\Team::all();
        $count = 1;

        foreach ($teams as $team) {
            for($i=1; $i<=11; $i++) {

                \App\Modules\TeamPlayer\TeamPlayer::create([
                    'team_id' => $team->id,
                    'player_id' => $count
                ]);
                $count++;
            }
        }
    }
}
