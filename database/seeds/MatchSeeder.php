<?php

use Illuminate\Database\Seeder;

class MatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Modules\Match\Match::create([
            'team1_id' => 1,
            'team2_id' => 2,
            'tournament_id' => 1,
        ]);
        \App\Modules\Match\Match::create([
            'team1_id' => 3,
            'team2_id' => 4,
            'tournament_id' => 1,
        ]);

        \App\Modules\ScoreCard\ScoreCard::create([
            'match_id' => 1,
            'innings' => 1
        ]);
        \App\Modules\ScoreCard\ScoreCard::create([
            'match_id' => 2,
            'innings' => 1
        ]);
    }
}
